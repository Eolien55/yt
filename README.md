# Shell/Dmenu/Rc Invidious Client

This allows to view videos, comments, playlists, channels, with
thumbnails, and everything, with dmenu.

Note: this was intended to be all-rc, but it was useless to plan9 folks,
partly because dmenu doesn't exist there.

# Installation

git clone, and put all scripts (bin) somewhere in your `PATH`.

ylesskey is excepted to be in ~/usr/lib. Put it wherever you want,
you just need to adapt `ycomments` so it knows its correct location.

You might also change the `root=` line of `ytmenu` to you real
`XDG_CACHE_HOME` (by default, ~/.cache)

# Dependencies

## Necessary

- mpv + youtube-dl/yt-dlp, to view videos.
- curl.
- jaq (from [json2tsv](https://codemadness.org/json2tsv.html))
- sponge (from [moreutils](http://joeyh.name/code/moreutils))
- Standard Unix utilities (sh, awk, sed, grep, etc.)
- xclip. You can use wl-clip, or alternatives, by editing `c`.

## For ytmenu

- nsxiv. You can use sxiv, you just need to change the nsxiv line in `ytmenu`.
- dmenu-like software. You need to change invidious and dmenud to use stuff like bemenu, or rofi.
- Rc. From either plan9port, 9base, or as a standalone program. Only needed for ytmenu
- A terminal emulator. By default, st. You need to change `tin` and `ytmenu` to use a different one.
- For `ycomments`: `fmt` and `tput`. These are standard, normally.
- GNU less, for `ycomment`'s keys

# Similar projects

- [pipe-viewer](https://github.com/trizen/pipe-viewer)

# License

Public domain.
